# ENV

python -m venv env

. env/bin/activate

pip install -r requirements.txt

# Run server

python manage.py runserver


# To migrate and load models

python manage.py makemigrations characters

and

python manage.py migrate

Load fixture:

python manage.py loaddata characters/fixtures/fixture.json


# Use website

To start site:

python manage.py runserver


To add superadmin:

python manage.py createsuperuser


/admin to admin the website ( add users, characters, artwork )

/artworks to see the list of artworks

/characters to see the list of characters

And if you want to create, update or delete artwork or character use :

* /artworks/create
* /artworks/update/```id```
* /artworks/delete/```id```
* /characters/create
* /characters/update/```id```
* /characters/delete/```id```

