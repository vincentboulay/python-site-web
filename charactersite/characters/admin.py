from django.contrib import admin

from .models import Artwork, Character

# Register your models here.

admin.site.register(Artwork)
admin.site.register(Character)
