from django.db import models

# Create your models here.

class Artwork(models.Model):
    name = models.CharField(max_length=200)
    artType = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    pub_date = models.DateField('date published')
    def __str__(self):
        return "%s: %s" % (self.name, self.artType)

class Character(models.Model):
    name = models.CharField(max_length=200)
    artwork = models.ForeignKey(Artwork, on_delete=models.CASCADE)
    description = models.CharField(max_length=200)
    def __str__(self):
        return "%s: %s" % (self.name, self.artwork)
