from django.urls import path

from . import views

urlpatterns = [
        path('', views.index, name='index'),
        path('characters/', views.CharacterListView.as_view(), name='character-list'),
        path('characters/<int:pk>/', views.CharacterDetailView.as_view(), name='character-detail'),
        path('characters/create/', views.CharacterCreateView.as_view(), name='character-create'),
        path('characters/update/<int:pk>/', views.CharacterUpdateView.as_view(), name='character-update'),
        path('characters/delete/<int:pk>/', views.CharacterDeleteView.as_view(), name='character-delete'),
        path('artworks/', views.ArtworkListView.as_view(), name='artwork-list'),
        path('artworks/<int:pk>/', views.ArtworkDetailView.as_view(), name='artwork-detail'),
        path('artworks/create/', views.ArtworkCreateView.as_view(), name='artwork-create'),
        path('artworks/update/<int:pk>/', views.ArtworkUpdateView.as_view(), name='artwork-update'),
        path('artworks/delete/<int:pk>/', views.ArtworkDeleteView.as_view(), name='artwork-delete'),
]
