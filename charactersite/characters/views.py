from django.shortcuts import render
from django.http import HttpResponse

from django.views.generic import DetailView, ListView, TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from characters import models

# Create your views here.

def index(request):
    return HttpResponse("Hello little guy.");

class CharacterListView(ListView):
    model = models.Character
    template_name = 'character_list.html'
    ordering = ('name', 'name')


class CharacterDetailView(DetailView):
    model = models.Character
    template_name = 'character_detail.html'

class ArtworkListView(ListView):
    model = models.Artwork
    template_name = 'artwork_list.html'
    ordering = ('name', 'name')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tmp = self.request.GET.get('type')
        context['type'] = tmp
        return context

    def get_queryset(self):
        tmp = self.request.GET.get('type')
        new_context = models.Artwork.objects.filter(artType=tmp) if tmp != None else super().get_queryset()
        return new_context

class ArtworkDetailView(DetailView):
    model = models.Artwork
    template_name = 'artwork_detail.html'

class ArtworkCreateView(CreateView):
    model = models.Artwork
    fields = ['name', 'artType', 'description', 'pub_date']
    template_name = 'artwork_form.html'
    success_url = "/artworks"

class ArtworkUpdateView(UpdateView):
    model = models.Artwork
    fields = ['name', 'artType', 'description', 'pub_date']
    template_name = 'artwork_update_form.html'
    success_url = "/artworks"

class ArtworkDeleteView(DeleteView):
    model = models.Artwork
    template_name = 'artwork_delete_form.html'
    success_url = "/artworks"

class CharacterCreateView(CreateView):
    model = models.Character
    fields = ['name', 'description', 'artwork']
    template_name = 'character_form.html'
    success_url = "/characters"

class CharacterUpdateView(UpdateView):
    model = models.Character
    fields = ['name', 'description', 'artwork']
    template_name = 'character_update_form.html'
    success_url = "/characters"

class CharacterDeleteView(DeleteView):
    model = models.Character
    template_name = 'character_delete_form.html'
    success_url = "/characters"